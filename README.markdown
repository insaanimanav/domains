# Domains managed by Free Software Community of India

This repository contains DNS-as-code configurations that is to be used
with [octoDNS] for domains that are managed by the
[Free Software Community of India]. All these domains are hosted on
DigitalOcean.

There are [other domains] that are registered on behalf of other
communities but are not managed by this community.

# Domain list

The list of domains that are managed with octoDNS can be seen within
the [`domains`] directory.

[octoDNS]: https://github.com/octodns/octodns
[Free Software Community of India]: https://fsci.in
[other domains]: https://wiki.fsci.org.in/index.php/Domains_Managed
[`domains`]: ./domains
